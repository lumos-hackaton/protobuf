#!/bin/bash

export FBACK_SRC=$GOPATH/src/bitbucket.org/lumos-hackaton/protobuf/src
export FBACK_GEN=$GOPATH/src/bitbucket.org/lumos-hackaton/protobuf/generated

mkdir $FBACK_GEN

build_proto() {
    local path=""
    if [[ $2 != "" ]]; then
        path="${2}/"
    fi

    protoc \
        -I=$FBACK_SRC \
        --proto_path=$GOPATH/src:$GOPATH/src \
        --micro_out=$GOPATH/src \
        --go_out=$GOPATH/src \
        --validate_out="lang=go:$GOPATH/src" \
        ${FBACK_SRC}/${path}/${1}

    echo "${FBACK_SRC}/${path}/${1}"

    if [[ $2 != "" ]]; then
        serviceArr=(${1//./ })

        cd $FBACK_GEN/${path}
        ../../proto-echo/bin ${serviceArr[0]}
    fi
}


build_proto shared.proto

arr=(auth)
for item in ${arr[*]}
do
    build_proto "${item}.proto" $item
done
